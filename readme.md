# README #

### кратко ###

1. необходимо создать конфиг plexupdate.conf из примера, заполнив его своими данными.
2. папке ./etc перед сборкой надо задать права на запись 
~~~~
chmod -R 777 /home/docker/plex/etc
~~~~

3. можно запустить 
~~~~
/home/docker/plex/docker-compose up -d
~~~~

4. Если удобнее без compose:

~~~~
 docker build -t rndsevsk/plex .

docker run -d --name=plex --net="host" --restart=on-failure:10 -v $PWD/etc/plex:/config -v /media/shaaare/Dwn/:/data  -p 32400:32400 rndsevsk/plex
~~~~