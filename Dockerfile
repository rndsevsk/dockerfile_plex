FROM phusion/baseimage
MAINTAINER rndsevsk
RUN rm -rf /etc/service/sshd /etc/my_init.d/00_regen_ssh_host_keys.sh
ENV DEBIAN_FRONTEND noninteractive
# Set correct environment variables
ENV HOME /root


# Use baseimage-docker's init system
CMD ["/sbin/my_init"]

#  config plexupdate
ADD plexupdate.conf /etc/plexupdate.conf
#ADD plexupdate.sh /opt/plexupdate.sh

# Install Plex # Install PlexUpdate
RUN apt-get -q update && apt-get -y upgrade && \
apt-get install -y wget apt-utils avahi-daemon && \
#wget -P /tmp "https://plex.tv/downloads/latest/1?channel=16&build=linux-ubuntu-x86_64&distro=ubuntu" && \
wget -P /opt "https://raw.githubusercontent.com/mrworf/plexupdate/master/plexupdate.sh"  && \
wget -P /opt "https://raw.githubusercontent.com/mrworf/plexupdate/master/plexupdate-core"  && \
chmod +x /opt/*.sh 
RUN bash /opt/plexupdate.sh -a -f 

# Clean apt & tmp
RUN apt-get clean && \
rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Fix a Debianism of plex's uid being 101
RUN usermod -u 777 plex  && \
usermod -g 100 plex

#Mappings and ports
VOLUME ["/config"]
VOLUME ["/data"]
VOLUME ["/data2"]
EXPOSE 32400

# config file 
ADD plexmediaserver /etc/default/plexmediaserver

# files to execute during container startup
ADD init/ /etc/my_init.d/
RUN chmod -v +x /etc/my_init.d/*.sh

# runit services
ADD run/ /etc/service/
RUN chmod -v +x /etc/service/*/run
